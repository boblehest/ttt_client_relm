use bincode::{deserialize, serialize_into};
use bytes::BytesMut;
use serde::Serialize;
use server::codec::length_delimited::{FramedReader, FramedWriter};
use server::messages::*;

use tictactoe_board::Position;

use gio::{InputStream, InputStreamExtManual, OutputStream, OutputStreamExt};
use relm::EventStream;
use relm::{Relm, Update, UpdateNew};

use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

// TODO Figure out the architecture we're going for here. We have multiple
// concerns in one place here. First off, there's the serializing/deserializing
// going on, then there's the actual storing of data (which, of course, mirrors
// the server state, so obviously this logic should be part of our
// client<->server library). So, everything mentioned thus far is NOT part of
// our core domain (but the domain of the client<->server library).
//
// Figure out how this fits into the MVC/MVP/MVVM/whatever pattern. There's some
// low hanging fruit here. There probably is a lot of low hanging fruit, but
// it requires actually bothering to think about the design for a non-zero
// amount of time.

// TODO Why is this called LobbyModel? It has data for everything, not just lobbies.
// WorldModel would be more appropriate. Or Realm, or Universe, or whatever.
pub struct LobbyModel {
    users: HashMap<UserId, String>,
    lobbies: HashMap<LobbyId, Lobby>,
    games: HashMap<GameId, Game>,
}

pub enum LobbyMessage {
    GameEvent { game: GameId, event: GameEvent },
    LobbyEvent { lobby: LobbyId, event: LobbyEvent },
    UserEvent { user: UserId, event: UserEvent },
}

#[derive(Clone)]
pub enum GameEvent {
    GameEnded { reason: GameResult },
    PlayerMoved { position: Position, is_self: bool },
}

pub enum LobbyEvent {
    Created,
    Destroyed,
    // TODO Why don't we include more info here? Did they join or leave? Who did?
    GuestChanged,
    Started(GameId),
    TitleChanged,
}

pub enum UserEvent {
    Connected,
    Disconnected,
}

impl LobbyModel {
    fn new() -> Self {
        LobbyModel {
            users: Default::default(),
            lobbies: Default::default(),
            games: Default::default(),
        }
    }

    pub fn get_lobbies(&self) -> &HashMap<LobbyId, Lobby> {
        &self.lobbies
    }

    pub fn get_users(&self) -> &HashMap<UserId, String> {
        &self.users
    }

    pub fn get_games(&self) -> &HashMap<GameId, Game> {
        &self.games
    }

    fn add_user(&mut self, id: UserId, name: String) -> LobbyMessage {
        self.users.insert(id, name);

        LobbyMessage::UserEvent {
            user: id,
            event: UserEvent::Connected,
        }
    }

    fn remove_user(&mut self, id: UserId) -> LobbyMessage {
        self.users.remove(&id);

        LobbyMessage::UserEvent {
            user: id,
            event: UserEvent::Disconnected,
        }
    }

    fn change_title(&mut self, lobby: LobbyId, new_title: String) -> LobbyMessage {
        self.lobbies.get_mut(&lobby).unwrap().title = new_title;

        LobbyMessage::LobbyEvent {
            lobby,
            event: LobbyEvent::TitleChanged,
        }
    }

    fn create_lobby(&mut self, id: LobbyId, new_lobby: Lobby) -> LobbyMessage {
        self.lobbies.insert(id, new_lobby);

        LobbyMessage::LobbyEvent {
            lobby: id,
            event: LobbyEvent::Created,
        }
    }

    fn destroy_lobby(&mut self, lobby: LobbyId) -> LobbyMessage {
        self.lobbies.remove(&lobby);

        LobbyMessage::LobbyEvent {
            lobby,
            event: LobbyEvent::Destroyed,
        }
    }

    fn add_guest(&mut self, lobby: LobbyId, user: UserId) -> LobbyMessage {
        self.lobbies.get_mut(&lobby).unwrap().add_guest(user);

        LobbyMessage::LobbyEvent {
            lobby,
            event: LobbyEvent::GuestChanged,
        }
    }

    fn remove_guest(&mut self, lobby: LobbyId) -> LobbyMessage {
        self.lobbies.get_mut(&lobby).unwrap().remove_guest();

        LobbyMessage::LobbyEvent {
            lobby,
            event: LobbyEvent::GuestChanged,
        }
    }

    fn start_game(&mut self, lobby: LobbyId, id: GameId) -> LobbyMessage {
        let lobby_obj = self.lobbies.remove(&lobby).unwrap();
        self.games.insert(id, lobby_obj.into_game());

        LobbyMessage::LobbyEvent {
            lobby,
            event: LobbyEvent::Started(id),
        }
    }

    fn end_game(&mut self, game: GameId, reason: GameResult) -> LobbyMessage {
        self.games.remove(&game).unwrap();
        LobbyMessage::GameEvent {
            game,
            event: GameEvent::GameEnded { reason },
        }
    }

    fn make_move(&mut self, game: GameId, position: Position, is_self: bool) -> LobbyMessage {
        // TODO Make actual changes to game board
        // Edit: WIP
        let game_obj = self.games.get_mut(&game).unwrap();
        game_obj
            .grid
            .place_mark(tictactoe_board::PlayerId::Circle, position);

        LobbyMessage::GameEvent {
            game,
            event: GameEvent::PlayerMoved { position, is_self },
        }
    }
}

// TODO Rename this to something more appropriate. Does it fit into the
// MVC (or M(V/VM)C) pattern?
pub struct UserModel {
    user_id: UserId,
    pending_requests: Vec<UserRequest>,
    lobby_model: Rc<RefCell<LobbyModel>>,
}

impl UserModel {
    fn new(user_id: UserId, lobby_model: Rc<RefCell<LobbyModel>>) -> Self {
        UserModel {
            user_id,
            pending_requests: Default::default(),
            lobby_model,
        }
    }

    fn change_title(&mut self, lobby: LobbyId, new_title: String) -> UserRequest {
        let request = UserRequest::ChangeTitle(lobby, new_title);
        self.pending_requests.push(request.clone());
        request
    }

    fn create_lobby(&mut self, title: String) -> UserRequest {
        let request = UserRequest::CreateLobby(title);
        self.pending_requests.push(request.clone());
        request
    }

    fn destroy_lobby(&mut self, lobby: LobbyId) -> UserRequest {
        let request = UserRequest::DestroyLobby(lobby);
        self.pending_requests.push(request.clone());
        request
    }

    fn join_lobby(&mut self, lobby: LobbyId) -> UserRequest {
        let request = UserRequest::JoinLobby(lobby);
        self.pending_requests.push(request.clone());
        request
    }

    fn kick_guest(&mut self, lobby: LobbyId) -> UserRequest {
        let request = UserRequest::KickGuest(lobby);
        self.pending_requests.push(request.clone());
        request
    }

    fn start_game(&mut self, lobby: LobbyId) -> UserRequest {
        let request = UserRequest::StartGame(lobby);
        self.pending_requests.push(request.clone());
        request
    }

    fn make_move(&mut self, game: GameId, position: Position) -> UserRequest {
        let request = UserRequest::MakeMove(game, position);
        self.pending_requests.push(request.clone());
        request
    }
}

// TODO Maybe wrap this enum in a struct so we don't have to worry
// about users trying to access its fields. The data is only for internal use
pub enum State {
    NotLoggedIn,
    LoggedIn(UserModel),
}

impl State {
    pub fn new() -> Self {
        State::NotLoggedIn
    }
}

// TODO Use IoC instead?
pub enum ClientMessage {
    LoggedIn(UserId, Rc<RefCell<LobbyModel>>),
    LobbyMessage(LobbyMessage),
}

pub struct Client {
    state: State,
    stream: EventStream<ClientMessage>,
    server_tx: OutputStream,
    writer: FramedWriter,
}

// TODO The `send` stuff is completely orthogonal to `recv`; this type should
// not do both.
impl Client {
    pub fn new(server_tx: OutputStream) -> Self {
        Client {
            state: State::new(),
            stream: EventStream::new(),
            server_tx,
            writer: FramedWriter::new(),
        }
    }

    pub fn change_title(&mut self, lobby: LobbyId, new_title: String) {
        // TODO Get rid of this shit? Preferably, these methods should only
        // be available after we've logged in.
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.change_title(lobby, new_title),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn create_lobby(&mut self, title: String) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.create_lobby(title),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn destroy_lobby(&mut self, lobby: LobbyId) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.destroy_lobby(lobby),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn join_lobby(&mut self, lobby: LobbyId) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.join_lobby(lobby),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn kick_guest(&mut self, lobby: LobbyId) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.kick_guest(lobby),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn login(&mut self, user_name: String) {
        let msg = server::messages::AnonRequest::Login { user_name };
        self.send(&msg);
    }

    pub fn start_game(&mut self, lobby: LobbyId) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.start_game(lobby),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    pub fn make_move(&mut self, game: GameId, position: Position) {
        let msg = match &mut self.state {
            State::LoggedIn(user_model) => user_model.make_move(game, position),
            _ => panic!("shit"),
        };
        self.send(&msg);
    }

    fn send<T: Serialize + std::fmt::Debug>(&mut self, message: &T) {
        let packet = self
            .writer
            .write_with(|w| serialize_into(w, message))
            .unwrap();

        debug!("Sending message {:?}", message);

        match self.server_tx.write_all(&packet, None) {
            Ok(_) => (),
            // TODO Do something about this. Going back to the "connect to server"
            // widget is probably the only appropriate thing right now.
            Err(e) => error!("Failed to write: {:?}", e),
        }
    }

    pub fn stream(&self) -> &EventStream<ClientMessage> {
        &self.stream
    }

    pub fn handle_message(&mut self, message: &[u8]) -> Result<(), bincode::Error> {
        let event = match self.state {
            State::NotLoggedIn => {
                let message = deserialize::<AnonResponse>(message)?;
                info!("Deserialized message: {:?}", message);
                let AnonResponse::Login(user_id) = message;

                let lobby_model = Rc::new(RefCell::new(LobbyModel::new()));
                self.state = State::LoggedIn(UserModel::new(user_id, lobby_model.clone()));

                Some(ClientMessage::LoggedIn(user_id, lobby_model))
            }
            State::LoggedIn(ref mut model) => {
                // TODO Looks like this logic belongs in the model:
                // model.handle_message(message);
                let message = deserialize::<UserResponse>(message)?;
                info!("Deserialized message: {:?}", message);

                let mut lobby_model = model.lobby_model.borrow_mut();
                match message {
                    UserResponse::ChangeTitle(result) => {
                        let (lobby, new_title) = match model.pending_requests.remove(0) {
                            UserRequest::ChangeTitle(lobby, new_title) => (lobby, new_title),
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            Some(lobby_model.change_title(lobby, new_title))
                        } else {
                            None
                        }
                    }
                    UserResponse::Concede(result) => {
                        let game = match model.pending_requests.remove(0) {
                            UserRequest::Concede(game) => game,
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            // TODO Remove dummy playerid value
                            Some(lobby_model.end_game(game, GameResult::PlayerConceded(0)))
                        } else {
                            None
                        }
                    }
                    UserResponse::CreateLobby(result) => {
                        let id = result.ok().unwrap();
                        let title = match model.pending_requests.remove(0) {
                            UserRequest::CreateLobby(title) => title,
                            _ => panic!(),
                        };
                        let new_lobby = Lobby::new(model.user_id, title);
                        Some(lobby_model.create_lobby(id, new_lobby))
                    }
                    UserResponse::DestroyLobby(result) => {
                        let lobby = match model.pending_requests.remove(0) {
                            UserRequest::DestroyLobby(lobby) => lobby,
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            Some(lobby_model.destroy_lobby(lobby))
                        } else {
                            None
                        }
                    }
                    UserResponse::JoinLobby(result) => {
                        let lobby = match model.pending_requests.remove(0) {
                            UserRequest::JoinLobby(lobby) => lobby,
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            Some(lobby_model.add_guest(lobby, model.user_id))
                        } else {
                            None
                        }
                    }
                    UserResponse::KickGuest(result) => {
                        let lobby = match model.pending_requests.remove(0) {
                            UserRequest::KickGuest(lobby) => lobby,
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            Some(lobby_model.remove_guest(lobby))
                        } else {
                            None
                        }
                    }
                    UserResponse::MakeMove(result) => {
                        let (game, position) = match model.pending_requests.remove(0) {
                            UserRequest::MakeMove(game, position) => (game, position),
                            _ => panic!(),
                        };
                        if result.is_ok() {
                            Some(lobby_model.make_move(game, position, true))
                        } else {
                            None
                        }
                    }
                    UserResponse::StartGame(result) => {
                        let lobby = match model.pending_requests.remove(0) {
                            UserRequest::StartGame(lobby) => lobby,
                            _ => panic!(),
                        };
                        match result {
                            Ok(id) => Some(lobby_model.start_game(lobby, id)),
                            Err(_) => None,
                        }
                    }
                    // Unicast messages above -^-
                    // Broadcast messages below -v-
                    UserResponse::GameFinished { game, reason } => {
                        Some(lobby_model.end_game(game, reason))
                    }
                    UserResponse::GameStarted { lobby, id } => {
                        Some(lobby_model.start_game(lobby, id))
                    }
                    UserResponse::LobbyCreated { id, title, owner } => {
                        let new_lobby = Lobby::new(owner, title);
                        Some(lobby_model.create_lobby(id, new_lobby))
                    }
                    UserResponse::LobbyDestroyed { lobby } => {
                        Some(lobby_model.destroy_lobby(lobby))
                    }
                    UserResponse::LobbyJoined { lobby, user } => {
                        Some(lobby_model.add_guest(lobby, user))
                    }
                    UserResponse::LobbyParted { lobby } => Some(lobby_model.remove_guest(lobby)),
                    UserResponse::LobbyTitleChanged { lobby, new_title } => {
                        Some(lobby_model.change_title(lobby, new_title))
                    }
                    UserResponse::OpponentMoved { game, position } => {
                        Some(lobby_model.make_move(game, position, false))
                    }
                    UserResponse::UserConnected { id, name } => {
                        debug!("User connected: {} / {}", id.0, name);
                        Some(lobby_model.add_user(id, name))
                    }
                    UserResponse::UserDisconnected { user } => {
                        debug!("User disconnected: {}", user.0);
                        Some(lobby_model.remove_user(user))
                    }
                }
                .map(ClientMessage::LobbyMessage)
            }
        };

        if let Some(event) = event {
            self.stream.emit(event);
        }

        Ok(())
    }
}

pub trait MessageHandler {
    fn on_receive(&mut self, buffer: &mut BytesMut);
}

pub struct ClientMessageHandler {
    client: Rc<RefCell<Client>>,
    reader: FramedReader,
}

impl ClientMessageHandler {
    pub fn new(client: Rc<RefCell<Client>>) -> Self {
        Self {
            client,
            reader: FramedReader::new(),
        }
    }
}

impl MessageHandler for ClientMessageHandler {
    fn on_receive(&mut self, buffer: &mut BytesMut) {
        while let Some(message) = self.reader.read(buffer) {
            self.client.borrow_mut().handle_message(&message).unwrap();
        }
    }
}

#[derive(Msg)]
pub enum Message {
    Read((BytesMut, usize)),
}

pub struct Component<H: MessageHandler> {
    server_rx: InputStream,
    relm: Relm<Component<H>>,
    buffer: BytesMut,
    handler: H,
}

const BUF_SIZE: usize = 256;

impl<H: MessageHandler> Update for Component<H> {
    type Model = (InputStream, H);
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::Read((mut buffer, num_bytes_read)) => {
                unsafe {
                    buffer.set_len(num_bytes_read);
                }
                self.buffer.unsplit(buffer);

                if num_bytes_read == 0 {
                    warn!("Connection has been closed.");
                    return;
                }

                self.handler.on_receive(&mut self.buffer);

                let leftovers = self.buffer.len();
                self.buffer.reserve(BUF_SIZE - leftovers); // Reclaim the original buffer
                assert_eq!(self.buffer.capacity(), BUF_SIZE);
                unsafe {
                    self.buffer.set_len(BUF_SIZE);
                }

                connect_async!(
                    self.server_rx,
                    read_async(self.buffer.split_off(leftovers), glib::PRIORITY_DEFAULT),
                    self.relm,
                    Message::Read
                );
            }
        }
    }
}

impl<H: MessageHandler> UpdateNew for Component<H> {
    fn new(relm: &Relm<Self>, (server_rx, handler): Self::Model) -> Self {
        let mut buffer = BytesMut::new();
        buffer.resize(BUF_SIZE, 0);

        connect_async!(
            server_rx,
            read_async(buffer.split_off(0), glib::PRIORITY_DEFAULT),
            relm,
            Message::Read
        );

        Component {
            server_rx,
            relm: relm.clone(),
            buffer,
            handler,
        }
    }
}
