use gtk::{GtkListStoreExt, TreeModelExt};

pub struct ListStoreRow<'a> {
    store: &'a gtk::ListStore,
    iter: gtk::TreeIter,
}

impl<'a> ListStoreRow<'a> {
    pub fn get_column(&self, column: i32) -> gtk::Value {
        self.store.get_value(&self.iter, column)
    }

    pub fn remove(&self) {
        self.store.remove(&self.iter);
    }
}

pub struct ListStoreIter<'a> {
    store: &'a gtk::ListStore,
    iter: Option<gtk::TreeIter>,
}

impl<'a> ListStoreIter<'a> {
    pub fn new(store: &'a gtk::ListStore) -> Self {
        let iter = store.get_iter_first();
        ListStoreIter { store, iter }
    }
}

impl<'a> Iterator for ListStoreIter<'a> {
    type Item = ListStoreRow<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.take() {
            Some(iter) => {
                let iter_current = iter.clone();
                if self.store.iter_next(&iter) {
                    self.iter = Some(iter);
                }
                Some(ListStoreRow {
                    store: self.store,
                    iter: iter_current,
                })
            }
            None => None,
        }
    }
}
