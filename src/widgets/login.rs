use crate::network::Client;

use gtk::{Align, ButtonExt, ContainerExt, EntryExt, Orientation, WidgetExt};

use relm::{Relm, Update, Widget};

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Msg)]
pub enum Message {
    Login(String),
}

pub struct Component {
    root: gtk::Box,
    client: Rc<RefCell<Client>>,
}

impl Update for Component {
    type Model = (Rc<RefCell<Client>>,);
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_relm: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            // TODO Use IoC? Use IoC.
            Message::Login(user_name) => {
                self.client.borrow_mut().login(user_name);
            }
        }
    }
}

impl Widget for Component {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.root.clone()
    }

    fn view(relm: &Relm<Self>, (client,): Self::Model) -> Self {
        let root = gtk::Box::new(Orientation::Vertical, 0);
        root.set_halign(Align::Center);
        root.set_valign(Align::Center);
        root.set_vexpand(true);

        let entry = gtk::Entry::new();
        entry.set_placeholder_text("Username");
        entry.show();
        root.add(&entry);

        let button = gtk::Button::new_with_label("Login");

        let stream = relm.stream().clone();

        button.connect_clicked(move |button| {
            button.set_sensitive(false);
            let username = entry.get_text().unwrap();
            stream.emit(Message::Login(username));
        });

        button.show();
        root.add(&button);

        root.show();

        Component { root, client }
    }
}
