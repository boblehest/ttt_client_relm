extern crate bincode;
extern crate bytes;
extern crate cairo;
extern crate gdk;
extern crate glib;
extern crate server;
extern crate tictactoe_board;

use tictactoe_board::{Game, GameResult, Grid, GridLine, PlayerId, Position};

use cairo::prelude::*;
use cairo::Matrix;
use gdk::EventMask;
use gtk::{self, prelude::*, ContainerExt, DrawingArea, FrameExt, Orientation, WidgetExt};
use relm::{Relm, Update, Widget};

use float_ord::FloatOrd;

use std::cell::RefCell;
use std::cmp::min;
use std::f64::consts::PI;
use std::rc::Rc;

const GRID_LINE_WIDTH: f64 = 0.1;

// make moving clones into closures more convenient
macro_rules! clone {
	(@param _) => ( _ );
	(@param $x:ident) => ( $x );
	($($n:ident),+ => move || $body:expr) => (
		{
			$( let $n = $n.clone(); )+
				move || $body
		}
	);
	($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
		{
			$( let $n = $n.clone(); )+
				move |$(clone!(@param $p),)+| $body
		}
	);
}

pub enum GameState {
    InProgress(Game),
    GameOver(GameResult, Grid),
}

impl GameState {
    fn get_grid(&self) -> &Grid {
        match self {
            GameState::InProgress(game) => game.get_grid(),
            GameState::GameOver(_, grid) => grid,
        }
    }
}

pub trait WidgetOutputPort {
    fn cell_clicked(&self, position: Position);

    fn on_leave(&self);

    fn get_game_state(&self) -> Rc<RefCell<GameState>> {
        unimplemented!()
    }
}

pub enum DisplayHoverHint {
    One(PlayerId),
    Both,
}

impl DisplayHoverHint {
    fn includes(&self, other: PlayerId) -> bool {
        use DisplayHoverHint::*;

        match self {
            One(p) => *p == other,
            Both => true,
        }
    }
}

#[derive(Msg)]
pub enum Message {
    OnPlayerMove { position: Position },
}

pub struct Component<O> {
    root: gtk::Box,
    heading: gtk::Label,
    event_context: Rc<RefCell<EventContext<O>>>,
    display_hover_hint: DisplayHoverHint,
    output_port: O,
}

fn player_id_to_str(pid: PlayerId) -> &'static str {
    match pid {
        PlayerId::Cross => "X",
        PlayerId::Circle => "O",
    }
}

impl<O: WidgetOutputPort> Component<O> {
    fn sync_heading_to_game_state(&self) {
        match &*self.output_port.get_game_state().borrow() {
            GameState::InProgress(game) => {
                let player_str = player_id_to_str(game.current_player());

                self.heading
                    .set_label(&format!("Current player: {}", player_str));
            }
            GameState::GameOver(result, _) => match result {
                GameResult::Winner { winner, .. } => {
                    let player_str = player_id_to_str(*winner);
                    self.heading.set_label(&format!("{} won!", player_str));
                }
                GameResult::Draw => {
                    self.heading.set_label("It's a draw");
                }
            },
        }
    }

    fn sync_to_game_state(&self) {
        self.sync_heading_to_game_state();
        self.event_context.borrow_mut().update_ui();

        match &*self.output_port.get_game_state().borrow() {
            GameState::InProgress(game) => {
                let current_player = game.current_player();
                if self.display_hover_hint.includes(current_player) {
                    self.event_context
                        .borrow_mut()
                        .set_hover_hint(current_player);
                } else {
                    self.event_context.borrow_mut().unset_hover_hint();
                }
            }
            GameState::GameOver(result, _) => match result {
                GameResult::Winner {
                    winner: _,
                    winning_line,
                } => {
                    let mut event_context = self.event_context.borrow_mut();
                    event_context.mark_winner(winning_line.clone());
                    event_context.unset_hover_hint();
                }
                GameResult::Draw => {}
            },
        }
    }
}

impl<O: WidgetOutputPort> Update for Component<O> {
    type Model = (O, DisplayHoverHint);
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::OnPlayerMove { .. } => {
                self.sync_to_game_state();
            }
        }
    }
}

impl<O: WidgetOutputPort + Clone + 'static> Widget for Component<O> {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.root.clone()
    }

    fn view(_relm: &Relm<Self>, (output_port, display_hover_hint): Self::Model) -> Self {
        let root = gtk::Box::new(Orientation::Vertical, 0);

        let heading = gtk::Label::new(None);
        root.add(&heading);

        let aspect_ratio = {
            let game_state = output_port.get_game_state();
            let game_state = game_state.borrow();
            let game_grid = game_state.get_grid();

            f32::from(game_grid.width()) / f32::from(game_grid.height())
        };
        let grid_wrapper = gtk::AspectFrame::new(None, 0.5, 0.0, aspect_ratio, false);
        grid_wrapper.set_shadow_type(gtk::ShadowType::None);
        root.add(&grid_wrapper);

        let drawing_area = DrawingArea::new();

        drawing_area.set_hexpand(true);
        drawing_area.set_vexpand(true);

        grid_wrapper.add(&drawing_area);

        drawing_area.add_events(
            (EventMask::BUTTON_PRESS_MASK
                | EventMask::BUTTON_RELEASE_MASK
                | EventMask::POINTER_MOTION_MASK
                | EventMask::LEAVE_NOTIFY_MASK)
                .bits() as i32,
        );

        let event_context = Rc::new(RefCell::new(EventContext::new(
            output_port.clone(),
            drawing_area.clone(),
        )));

        drawing_area.connect_size_allocate(clone!(event_context => move |_, _| {
            event_context.borrow_mut().make_dirty();
        }));

        drawing_area.connect_draw(clone!(event_context => move |_widget, cr| {
            let mut event_context = event_context.borrow_mut();
            cr.set_matrix(event_context.matrix());

            event_context.draw_board_outline(&cr);
            event_context.draw_hover_hint(&cr);
            event_context.draw_winner_line(&cr);
            event_context.draw_board_marks(&cr);

            Inhibit(false)
        }));

        drawing_area.connect_leave_notify_event(clone!(event_context => move |_, _| {
            event_context.borrow_mut().set_hover_position(None);

            Inhibit(false)
        }));

        drawing_area.connect_motion_notify_event(clone!(event_context => move |_da, e| {
            let mut event_context = event_context.borrow_mut();
            let (px_x, px_y) = e.get_position();
            let coords = event_context.px_to_grid_coordinates([px_x, px_y]);
            event_context.set_hover_position(coords);

            Inhibit(false)
        }));

        drawing_area.connect_button_press_event(
            clone!(event_context, output_port => move |_da, e| {
                if e.get_button() == 1 && e.get_event_type() == gdk::EventType::ButtonPress {
                    let (px_x, px_y) = e.get_position();
                    if let Some(pos) = event_context.borrow().px_to_grid_coordinates([px_x, px_y]) {
                        output_port.cell_clicked(pos);
                    }
                }

                Inhibit(false)
            }),
        );

        let exit_button = gtk::Button::new_with_label("Leave game");

        exit_button.connect_clicked(clone!(output_port => move |_| {
            output_port.on_leave();
        }));

        root.add(&exit_button);

        root.show_all();

        let component = Component {
            root,
            heading,
            event_context,
            output_port,
            display_hover_hint,
        };

        component.sync_to_game_state();
        component
    }
}

struct EventContext<O> {
    output_port: O,
    widget: DrawingArea,
    matrix: Option<Matrix>,
    inv_matrix: Matrix, // optimization
    hover_hint: Option<PlayerId>,
    hover_position: Option<Position>,
    winner_line: Option<GridLine>,
}

impl<O: WidgetOutputPort> EventContext<O> {
    fn new(output_port: O, widget: DrawingArea) -> Self {
        EventContext {
            output_port,
            widget,
            matrix: None,
            inv_matrix: Matrix::identity(),
            hover_hint: None,
            hover_position: None,
            winner_line: None,
        }
    }

    fn update_ui(&mut self) {
        self.check_hover_position();
    }

    fn set_hover_hint(&mut self, hint: PlayerId) {
        self.hover_hint = Some(hint);
        self.widget.queue_draw();
    }

    fn unset_hover_hint(&mut self) {
        self.hover_hint = None;
        self.widget.queue_draw();
    }

    fn mark_winner(&mut self, path: GridLine) {
        self.winner_line = Some(path);
        self.widget.queue_draw();
    }

    fn make_dirty(&mut self) {
        self.matrix = None;
    }

    fn px_to_grid_coordinates(&self, pixel_coords: [f64; 2]) -> Option<Position> {
        let (x, y) = self
            .inv_matrix
            .transform_distance(pixel_coords[0], pixel_coords[1]);
        let (x, y) = (x as u8, y as u8);

        let game_state = self.output_port.get_game_state();
        let game_state = game_state.borrow();
        let grid = game_state.get_grid();

        if x < grid.width() && y < grid.height() {
            Some((x, y).into())
        } else {
            None
        }
    }

    fn check_hover_position(&mut self) {
        if let Some(pos) = self.hover_position {
            let game_state = self.output_port.get_game_state();
            let game_state = game_state.borrow();
            let grid = game_state.get_grid();

            if grid[pos].is_some() {
                self.hover_position = None;
                self.widget.queue_draw();
            }
        }
    }

    fn set_hover_position(&mut self, coords: Option<Position>) {
        self.hover_position = coords.and_then(|c| {
            let game_state = self.output_port.get_game_state();
            let game_state = game_state.borrow();
            let grid = game_state.get_grid();
            if grid[c].is_none() {
                Some(c)
            } else {
                None
            }
        });
        self.widget.queue_draw();
    }

    fn get_hover_position(&self) -> Option<Position> {
        self.hover_position
    }

    fn matrix(&mut self) -> Matrix {
        if self.matrix.is_none() {
            let game_state = self.output_port.get_game_state();
            let game_state = game_state.borrow();
            let grid = game_state.get_grid();
            let logical_width = f32::from(grid.width());
            let logical_height = f32::from(grid.height());

            let alloc = self.widget.get_allocation();
            let (physical_width, physical_height) = (alloc.width as f32, alloc.height as f32);

            let scale = f64::from(
                min(
                    FloatOrd(physical_width / logical_width),
                    FloatOrd(physical_height / logical_height),
                )
                .0,
            );

            let mut matrix = Matrix::identity();
            matrix.translate(f64::from(alloc.x), f64::from(alloc.y));
            matrix.scale(scale, scale);

            self.inv_matrix = matrix;
            self.inv_matrix.invert();

            self.matrix = Some(matrix);
        }

        self.matrix.unwrap()
    }

    fn draw_winner_line(&self, cr: &cairo::Context) {
        if let Some(winner_line) = &self.winner_line {
            for position in winner_line {
                cr.set_source_rgba(0.0, 0.8, 0.4, 1.0);
                self.draw_cell_background(cr, position);
            }
        }
    }

    fn draw_board_outline(&self, cr: &cairo::Context) {
        cr.set_source_rgba(1.0, 0.0, 0.0, 1.0);
        cr.set_line_width(GRID_LINE_WIDTH);

        let game_state = self.output_port.get_game_state();
        let game_state = game_state.borrow();
        let grid = game_state.get_grid();

        for x in 0..=grid.width() {
            cr.move_to(f64::from(x), 0.0);
            cr.line_to(f64::from(x), f64::from(grid.height()));
        }
        for y in 0..=grid.height() {
            cr.move_to(0.0, f64::from(y));
            cr.line_to(f64::from(grid.width()), f64::from(y));
        }
        cr.stroke();
    }

    fn draw_board_marks(&self, cr: &cairo::Context) {
        cr.set_line_width(GRID_LINE_WIDTH);

        let game_state = self.output_port.get_game_state();
        let game_state = game_state.borrow();
        let grid = game_state.get_grid();

        for (pos, player) in grid.iter_occupied() {
            match player {
                PlayerId::Cross => {
                    cr.set_source_rgb(0.3, 0.3, 0.6);
                    draw_cross(cr, pos);
                }
                PlayerId::Circle => {
                    cr.set_source_rgb(0.2, 0.6, 0.3);
                    draw_circle(cr, pos);
                }
            }
        }
    }

    fn draw_cell_background(&self, cr: &cairo::Context, pos: Position) {
        let (x, y) = (f64::from(pos.0), f64::from(pos.1));

        let line_width = GRID_LINE_WIDTH;

        cr.rectangle(
            x + line_width * 0.5,
            y + line_width * 0.5,
            1.0 - line_width,
            1.0 - line_width,
        );
        cr.fill();
    }

    fn draw_hover_hint(&self, cr: &cairo::Context) {
        if let (Some(hover_hint), Some(hover_position)) =
            (self.hover_hint, self.get_hover_position())
        {
            cr.set_source_rgba(0.2, 0.2, 0.2, 0.2);
            self.draw_cell_background(cr, hover_position);
            cr.set_source_rgba(0.5, 0.5, 0.2, 0.5);
            draw_player_mark(cr, hover_hint, hover_position);
        }
    }
}

fn draw_player_mark(cr: &cairo::Context, pid: PlayerId, pos: Position) {
    match pid {
        PlayerId::Cross => draw_cross(cr, pos),
        PlayerId::Circle => draw_circle(cr, pos),
    }
}

fn draw_cross(cr: &cairo::Context, pos: Position) {
    let (x, y) = (f64::from(pos.0), f64::from(pos.1));
    cr.move_to(x + 0.2, y + 0.2);
    cr.line_to(x + 0.8, y + 0.8);
    cr.move_to(x + 0.2, y + 0.8);
    cr.line_to(x + 0.8, y + 0.2);

    cr.stroke();
}

fn draw_circle(cr: &cairo::Context, pos: Position) {
    let (x, y) = (f64::from(pos.0), f64::from(pos.1));
    cr.arc(x + 0.5, y + 0.5, 0.3, 0., PI * 2.);
    cr.stroke();
}
