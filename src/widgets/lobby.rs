use crate::network::{Client, ClientMessage, LobbyEvent, LobbyMessage, LobbyModel};

use server::messages::LobbyId;

use relm::{Relm, Update, Widget};

use gtk::{
    self, BinExt, ButtonExt, Cast, ContainerExt, DialogExt, EntryExt, LabelExt, Orientation,
    WidgetExt,
};

use std::cell::RefCell;
use std::rc::Rc;

pub enum Request {
    ChangeTitle(String),
    DestroyLobby,
    KickGuest,
    StartGame,
}

#[derive(Msg)]
pub enum Message {
    GuestChanged,
    Request(Request),
    TitleChanged,
}

struct AdminButtons {
    kick: gtk::Button,
    start: gtk::Button,
}

pub struct Component {
    root: gtk::Box,
    title_label: gtk::Label,
    admin_buttons: Option<AdminButtons>,
    opponent_label: gtk::Label,

    client: Rc<RefCell<Client>>,
    lobby_model: Rc<RefCell<LobbyModel>>,
    lobby: LobbyId,
}

impl Update for Component {
    type Model = (
        Rc<RefCell<Client>>,
        Rc<RefCell<LobbyModel>>,
        LobbyId,
        gtk::Window,
        bool,
    );
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::GuestChanged => {
                debug!("Guest changed");
                let lobby_model = self.lobby_model.borrow();
                let guest = lobby_model
                    .get_lobbies()
                    .get(&self.lobby)
                    .map(|lobby| &lobby.guest)
                    .unwrap();

                let guest_name = if let Some(guest) = guest {
                    lobby_model.get_users().get(&guest).as_ref().unwrap()
                } else {
                    ""
                };
                self.opponent_label.set_label(guest_name);

                if let Some(b) = &self.admin_buttons {
                    b.kick.set_sensitive(guest.is_some());
                    b.start.set_sensitive(guest.is_some());
                }
            }
            Message::Request(request) => {
                match request {
                    Request::ChangeTitle(new_title) => {
                        self.client.borrow_mut().change_title(self.lobby, new_title)
                    }
                    Request::DestroyLobby => self.client.borrow_mut().destroy_lobby(self.lobby),
                    Request::KickGuest => self.client.borrow_mut().kick_guest(self.lobby),
                    Request::StartGame => self.client.borrow_mut().start_game(self.lobby),
                };
            }
            Message::TitleChanged => {
                let lobby_model = self.lobby_model.borrow();
                let title = lobby_model
                    .get_lobbies()
                    .get(&self.lobby)
                    .map(|lobby| &lobby.title)
                    .unwrap();
                self.title_label.set_text(title);
            }
        }
    }
}

impl Widget for Component {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.root.clone()
    }

    fn view(
        relm: &Relm<Self>,
        (client, lobby_model, lobby, window, is_admin): Self::Model,
    ) -> Self {
        let root = gtk::Box::new(Orientation::Vertical, 0);

        // TODO FIXME Each and every lobby widget will be listening here.
        // That's a bit ugly, though obviously not something that'll incur
        // a noticable hit to performance.
        // Edit: Maybe the main component should just send the messages
        // to the appropriate lobby component?
        let stream = relm.stream().clone();
        client.borrow().stream().observe(move |msg| {
            if let ClientMessage::LobbyMessage(msg) = msg {
                match msg {
                    LobbyMessage::LobbyEvent {
                        lobby: lobby_id,
                        event,
                    } if *lobby_id == lobby => match event {
                        LobbyEvent::TitleChanged => {
                            stream.emit(Message::TitleChanged);
                        }
                        LobbyEvent::GuestChanged => {
                            stream.emit(Message::GuestChanged);
                        }
                        _ => (),
                    },
                    _ => (),
                }
            }
        });

        // Setting: Title
        let title_label = {
            let lobby_model = lobby_model.borrow();
            let lobby_title = &lobby_model.get_lobbies().get(&lobby).unwrap().title;

            let title_row = gtk::Box::new(Orientation::Horizontal, 0);
            root.add(&title_row);
            let label = gtk::Label::new("Title: ");
            title_row.add(&label);

            if is_admin {
                let title_dialog = gtk::MessageDialog::new(
                    Some(&window),
                    gtk::DialogFlags::all(),
                    gtk::MessageType::Question,
                    gtk::ButtonsType::OkCancel,
                    "Enter a new name for the lobby",
                );

                title_dialog.connect_delete_event(|dialog, _event| {
                    dialog.hide();
                    gtk::Inhibit(true)
                });

                let dialog_content = title_dialog.get_content_area();
                let lobby_title_entry = gtk::Entry::new();
                lobby_title_entry.set_placeholder_text("Lobby title");
                lobby_title_entry.show();
                dialog_content.add(&lobby_title_entry);

                let stream = relm.stream().clone();
                title_dialog.connect_response(move |dialog, response| {
                    if let gtk::ResponseType::Ok = response.into() {
                        let new_title = lobby_title_entry.get_text().unwrap();
                        info!("Trying to set title to {}", new_title);
                        stream.emit(Message::Request(Request::ChangeTitle(new_title)));
                    }
                    dialog.hide();
                    lobby_title_entry.set_text("");
                });

                let button = gtk::Button::new_with_label(&lobby_title);
                button.connect_clicked(move |_| title_dialog.show());

                title_row.add(&button);

                button
                    .get_child()
                    .expect("get_child")
                    .downcast::<gtk::Label>()
                    .expect("downcast")
            } else {
                let label = gtk::Label::new(lobby_title.as_str());
                title_row.add(&label);
                label
            }
        };

        // Opponent (name + kick button)
        // TODO Set opponent name if we're joining a server (with an admin in it already)
        let label = gtk::Label::new("Opponent: ");

        let opponent_label = {
            let lobby_model = lobby_model.borrow();

            let opponent_label_text = if is_admin {
                ""
            } else {
                let admin_user_id = &lobby_model.get_lobbies().get(&lobby).unwrap().owner;
                let admin_user_name = lobby_model.get_users().get(&admin_user_id).unwrap();
                admin_user_name
            };

            gtk::Label::new(opponent_label_text)
        };
        let row = gtk::Box::new(Orientation::Horizontal, 0);

        row.add(&label);
        row.add(&opponent_label);

        let kick_button = if is_admin {
            let kick_button = gtk::Button::new_with_label("Kick player");
            kick_button.set_sensitive(false);

            connect!(
                relm,
                kick_button,
                connect_clicked(_),
                Message::Request(Request::KickGuest)
            );

            row.add(&kick_button);

            Some(kick_button)
        } else {
            None
        };

        root.add(&row);

        // Destroy lobby
        if is_admin {
            let button = gtk::Button::new_with_label("Destroy lobby");
            connect!(
                relm,
                button,
                connect_clicked(_),
                Message::Request(Request::DestroyLobby)
            );
            root.add(&button);
        };

        // Start game
        let start_button = if is_admin {
            let button = gtk::Button::new_with_label("Start game");
            button.set_sensitive(false);
            connect!(
                relm,
                button,
                connect_clicked(_),
                Message::Request(Request::StartGame)
            );
            root.add(&button);
            Some(button)
        } else {
            None
        };

        root.show_all();

        let admin_buttons =
            kick_button.and_then(|kick| start_button.map(|start| AdminButtons { kick, start }));

        Component {
            root,
            title_label,
            admin_buttons,
            opponent_label,

            client,
            lobby_model,
            lobby,
        }
    }
}
