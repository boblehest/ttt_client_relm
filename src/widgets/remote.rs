use gio::{IOStreamExt, SocketConnection};
use gtk::{prelude::*, BoxExt, Cast, ContainerExt, IsA, NotebookExt, WidgetExt};

use relm::{ContainerWidget, EventStream, Relm, Update, Widget};

use crate::network::{self, Client, LobbyModel};
use crate::widgets::*;
use server::messages::{GameId, LobbyId, UserId};
use tictactoe_board::Position;

use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

// Used to store handles to any kind of component/widget (for widget-switching
// where we don't care about what sort of widget we're showing/hiding.)
trait WidgetWrapper<T> {
    fn remove(self: Box<Self>, container: &T);
}

impl<T: Widget, U> WidgetWrapper<U> for relm::Component<T>
where
    U: IsA<gtk::Widget> + IsA<gtk::Container> + IsA<gtk::Object> + Clone,
{
    fn remove(self: Box<Self>, container: &U) {
        relm::ContainerWidget::remove_widget::<T>(container, *self);
    }
}

#[derive(Clone)]
struct GameOutput {
    game: GameId,
    client: Rc<RefCell<Client>>,
}

impl GameOutput {
    fn new(game: GameId, client: Rc<RefCell<Client>>) -> Self {
        Self { game, client }
    }
}

impl game::WidgetOutputPort for GameOutput {
    fn cell_clicked(&self, position: Position) {
        // TODO Only make move if the cell is vacant
        self.client.borrow_mut().make_move(self.game, position);
    }

    fn on_leave(&self) {}
}

#[derive(Msg)]
pub enum Message {
    GameEvent(server::messages::GameId, network::GameEvent),
    GameStarted(LobbyId, GameId),
    JoinedLobby(LobbyId, bool),
    LoggedIn(UserId, Rc<RefCell<LobbyModel>>),
    // Emitted when we left* a lobby _if_ we were in it (it might also be
    // emitted for lobbies we aren't in at all. But will not be emitted for
    // lobbies we are in but did not leave.)
    // *Either voluntary, or forcibly (by being kicked, or the lobby being closed)
    // TODO Clean this up
    MaybePartedLobby(LobbyId),
    TitleChanged(LobbyId),
}

pub struct Component {
    notebook: gtk::Notebook,
    relm: Relm<Component>,

    #[allow(dead_code)]
    server: SocketConnection,
    client: Rc<RefCell<Client>>,
    lobby_model: Option<Rc<RefCell<LobbyModel>>>,

    main_tab: gtk::Box,
    main_tab_component: Option<Box<WidgetWrapper<gtk::Box>>>,

    lobby_tabs: HashMap<LobbyId, (Box<WidgetWrapper<gtk::Notebook>>, gtk::Label)>,
    game_tabs: HashMap<
        GameId,
        (
            Box<WidgetWrapper<gtk::Notebook>>,
            EventStream<game::Message>,
        ),
    >,
}

impl Update for Component {
    type Model = (SocketConnection,);
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_relm: &Relm<Self>, model: Self::ModelParam) -> Self::Model {
        model
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::GameEvent(game, event) => {
                if let network::GameEvent::PlayerMoved { position, .. } = event {
                    if let Some((_, stream)) = self.game_tabs.get(&game) {
                        stream.emit(game::Message::OnPlayerMove { position });
                    }
                }
            }
            Message::GameStarted(lobby_id, game_id) => {
                if let Some((lobby_tab, _)) = self.lobby_tabs.remove(&lobby_id) {
                    lobby_tab.remove(&self.notebook);

                    let output = GameOutput::new(game_id, self.client.clone());

                    let component = ContainerWidget::add_widget::<game::Component<GameOutput>>(
                        &self.notebook,
                        (output, game::DisplayHoverHint::Both),
                    );

                    let lobby_model = self.lobby_model.as_ref().unwrap().borrow();
                    let game_title = &lobby_model.get_games()[&game_id].title;

                    self.notebook
                        .set_tab_label_text(component.widget(), game_title);

                    let new_page_num = self.notebook.page_num(component.widget());
                    self.notebook.set_current_page(new_page_num);
                    let stream = component.stream().clone();

                    self.game_tabs
                        .insert(game_id, (Box::new(component), stream));
                }
            }
            Message::JoinedLobby(lobby, is_admin) => {
                let component = relm::ContainerWidget::add_widget::<lobby::Component>(
                    &self.notebook,
                    (
                        self.client.clone(),
                        self.lobby_model.as_ref().unwrap().clone(),
                        lobby,
                        self.notebook.get_toplevel().unwrap().downcast().unwrap(),
                        is_admin,
                    ),
                );

                let lobby_model = self.lobby_model.as_ref().unwrap().borrow();
                let lobby_title = &lobby_model.get_lobbies()[&lobby].title;

                self.notebook
                    .set_tab_label_text(component.widget(), lobby_title);

                let tab_label = {
                    let widget = component.widget();
                    let new_page_num = self.notebook.page_num(widget);
                    self.notebook.set_current_page(new_page_num);

                    self.notebook
                        .get_tab_label(widget)
                        .expect("get_tab_label")
                        .downcast::<gtk::Label>()
                        .expect("downcast")
                };

                // Store tab label so we can update it whenever the lobby
                // title changes.
                self.lobby_tabs
                    .insert(lobby, (Box::new(component), tab_label));
            }
            Message::LoggedIn(user_id, lobby_model) => {
                self.main_tab_component
                    .take()
                    .unwrap()
                    .remove(&self.main_tab);

                {
                    let stream = self.relm.stream().clone();
                    let lobby_model = lobby_model.clone();
                    self.client.borrow().stream().observe(move |msg| {
                        if let network::ClientMessage::LobbyMessage(msg) = msg {
                            match *msg {
                                network::LobbyMessage::GameEvent {
                                    game: game_id,
                                    ref event,
                                } => {
                                    stream.emit(Message::GameEvent(game_id, event.clone()));
                                }
                                network::LobbyMessage::UserEvent { .. } => {}
                                network::LobbyMessage::LobbyEvent {
                                    lobby: lobby_id,
                                    ref event,
                                } => match *event {
                                    network::LobbyEvent::Created => {
                                        if lobby_model
                                            .borrow()
                                            .get_lobbies()
                                            .get(&lobby_id)
                                            .map(|lobby| lobby.owner == user_id)
                                            .unwrap()
                                        {
                                            stream.emit(Message::JoinedLobby(lobby_id, true));
                                        }
                                    }
                                    network::LobbyEvent::Destroyed => {
                                        stream.emit(Message::MaybePartedLobby(lobby_id));
                                    }
                                    network::LobbyEvent::GuestChanged => {
                                        if let Some(lobby) =
                                            lobby_model.borrow().get_lobbies().get(&lobby_id)
                                        {
                                            match lobby.guest {
                                                Some(guest) => {
                                                    if guest == user_id {
                                                        stream.emit(Message::JoinedLobby(
                                                            lobby_id, false,
                                                        ));
                                                    }
                                                }
                                                None => {
                                                    if lobby.owner != user_id {
                                                        stream.emit(Message::MaybePartedLobby(
                                                            lobby_id,
                                                        ));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    network::LobbyEvent::Started(game_id) => {
                                        stream.emit(Message::GameStarted(lobby_id, game_id));
                                    }
                                    network::LobbyEvent::TitleChanged => {
                                        stream.emit(Message::TitleChanged(lobby_id));
                                    }
                                },
                            }
                        }
                    });
                }

                let component = relm::ContainerWidget::add_widget::<lobby_list::Component>(
                    &self.main_tab,
                    (
                        self.client.clone(),
                        lobby_model.clone(),
                        user_id,
                        self.notebook.get_toplevel().unwrap().downcast().unwrap(),
                    ),
                );

                self.main_tab_component = Some(Box::new(component));
                self.lobby_model = Some(lobby_model);
            }
            Message::MaybePartedLobby(lobby_id) => {
                if let Some((tab, _)) = self.lobby_tabs.remove(&lobby_id) {
                    tab.remove(&self.notebook);
                }
            }
            Message::TitleChanged(lobby_id) => {
                if let Some((_, tab_label)) = self.lobby_tabs.get(&lobby_id) {
                    let lobby_model = self.lobby_model.as_ref().unwrap().borrow();
                    let title = lobby_model
                        .get_lobbies()
                        .get(&lobby_id)
                        .map(|lobby| &lobby.title)
                        .unwrap();

                    tab_label.set_label(title);
                }
            }
        }
    }
}

impl Widget for Component {
    type Root = gtk::Notebook;

    fn root(&self) -> Self::Root {
        self.notebook.clone()
    }

    fn view(relm: &Relm<Self>, (server,): Self::Model) -> Self {
        let root = gtk::Notebook::new();
        root.set_scrollable(true);

        let main_tab = gtk::Box::new(gtk::Orientation::Vertical, 0);
        main_tab.connect_add(|b, w| b.set_child_packing(w, true, true, 0, gtk::PackType::Start));
        main_tab.show();
        root.add(&main_tab);
        root.set_tab_label_text(&main_tab, "Online");

        let client = network::Client::new(server.get_output_stream().unwrap());

        let stream = relm.stream().clone();
        client.stream().set_callback(move |msg| {
            if let network::ClientMessage::LoggedIn(user_id, lobby_model) = msg {
                stream.emit(Message::LoggedIn(user_id, lobby_model));
            }
        });

        let client = Rc::new(RefCell::new(client));

        let client_message_handler = network::ClientMessageHandler::new(client.clone());

        // TODO We're dropping this here. It still lives on forever.
        // We should store this once upstream fixes this bug :) until
        // then, we're being lazy as fuck and allowing it to drop.
        let _network = relm::execute::<network::Component<_>>((
            server.get_input_stream().unwrap(),
            client_message_handler,
        ));

        let component =
            relm::ContainerWidget::add_widget::<login::Component>(&main_tab, (client.clone(),));

        root.show_all();

        Self {
            notebook: root,
            relm: relm.clone(),
            server,
            client,
            lobby_model: None,

            main_tab,
            main_tab_component: Some(Box::new(component)),

            lobby_tabs: HashMap::new(),
            game_tabs: HashMap::new(),
        }
    }
}
