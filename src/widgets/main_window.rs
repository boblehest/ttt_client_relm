use crate::widgets::*;
use game::GameState;

use tictactoe_board::{Game, Grid, Position};

use gio::{SocketClient, SocketClientExt, SocketConnection};
use gtk::{prelude::*, ContainerExt, NotebookExt, WidgetExt};
use relm::{EventStream, Relm, Update, Widget};
use slab::Slab;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Clone)]
struct LocalGameOutput {
    game: Rc<RefCell<GameState>>,
    game_id: usize,
    parent_widget_stream: EventStream<Message>,
}

impl LocalGameOutput {
    fn new(game: Game, game_id: usize, parent_widget_stream: EventStream<Message>) -> Self {
        Self {
            game: Rc::new(RefCell::new(GameState::InProgress(game))),
            game_id,
            parent_widget_stream,
        }
    }
}

impl game::WidgetOutputPort for LocalGameOutput {
    fn cell_clicked(&self, position: Position) {
        let result = match *self.game.borrow_mut() {
            GameState::InProgress(ref mut game) => {
                if game.get_grid()[position].is_some() {
                    return;
                }
                game.make_move(position)
            }
            _ => return,
        };

        if let Some(result) = result {
            take_mut::take(&mut *self.game.borrow_mut(), |gs| {
                let grid = match gs {
                    GameState::InProgress(game) => game.into_grid(),
                    _ => unreachable!(),
                };
                GameState::GameOver(result, grid)
            });
        }

        self.parent_widget_stream.emit(Message::OnPlayerMoved {
            game_id: self.game_id,
            position,
        });
    }

    fn on_leave(&self) {
        self.parent_widget_stream
            .emit(Message::OnExitOfflineGame(self.game_id));
    }

    fn get_game_state(&self) -> Rc<RefCell<GameState>> {
        self.game.clone()
    }
}

#[derive(Msg)]
pub enum Message {
    OnConnected(SocketConnection),
    OnConnectFailed(gtk::Error),
    OnGoOnline,
    OnExitOfflineGame(usize),
    OnStartOfflineGame,
    OnPlayerMoved { game_id: usize, position: Position },
    Quit,
}

// SocketConnection does not implement send, but relm/examples/http does it
// exactly like this, so since I'm already using relm, I might as well copy
// the official methods without making my program less correct.
unsafe impl Send for Message {}

pub struct Component {
    window: gtk::Window,
    notebook: gtk::Notebook,
    offline_game_tabs: Slab<relm::Component<game::Component<LocalGameOutput>>>,
    remote_tabs: Vec<relm::Component<remote::Component>>,
    relm: Relm<Component>,
}

impl Update for Component {
    type Model = ();
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_relm: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::OnConnected(server) => {
                let component = relm::ContainerWidget::add_widget::<remote::Component>(
                    &self.notebook,
                    (server,),
                );
                self.notebook
                    .set_tab_label_text(component.widget(), "Online");
                self.remote_tabs.push(component);
            }
            Message::OnConnectFailed(_e) => {}
            Message::OnGoOnline => {
                let socket = SocketClient::new();
                connect_async!(
                    socket,
                    connect_to_host_async("[::]", 1337),
                    self.relm,
                    Message::OnConnected,
                    Message::OnConnectFailed
                );
            }
            Message::OnExitOfflineGame(game_id) => {
                relm::ContainerWidget::remove_widget(
                    &self.notebook,
                    self.offline_game_tabs.remove(game_id),
                );
            }
            Message::OnStartOfflineGame => {
                let entry = self.offline_game_tabs.vacant_entry();
                let game = Game::new(Grid::new(3, 3));
                let output = LocalGameOutput::new(game, entry.key(), self.relm.stream().clone());

                let component = relm::ContainerWidget::add_widget::<game::Component<LocalGameOutput>>(
                    &self.notebook,
                    (output, game::DisplayHoverHint::Both),
                );

                self.notebook.set_tab_label_text(
                    component.widget(),
                    &format!("Local game #{}", entry.key()),
                );

                let new_page_num = self.notebook.page_num(component.widget());
                self.notebook.set_current_page(new_page_num);

                entry.insert(component);
            }
            Message::OnPlayerMoved { game_id, position } => {
                let component = self.offline_game_tabs.get(game_id).unwrap();
                component
                    .stream()
                    .emit(game::Message::OnPlayerMove { position });
            }
            Message::Quit => gtk::main_quit(),
        }
    }
}

impl main_menu::UserActionHandler for relm::EventStream<Message> {
    fn on_start_hotseat(&self) {
        self.emit(Message::OnStartOfflineGame);
    }

    fn on_go_online(&self) {
        self.emit(Message::OnGoOnline);
    }

    fn on_exit(&self) {
        self.emit(Message::Quit);
    }
}

impl Widget for Component {
    type Root = gtk::Window;

    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    fn view(relm: &Relm<Self>, _model: Self::Model) -> Self {
        let window = gtk::Window::new(gtk::WindowType::Toplevel);

        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Some(Message::Quit), gtk::Inhibit(false))
        );

        let notebook = gtk::Notebook::new();
        notebook.set_scrollable(true);
        notebook.show();
        window.add(&notebook);

        let mm = relm::ContainerWidget::add_widget::<main_menu::Component<_>>(
            &notebook,
            (relm.stream().clone(),),
        );
        notebook.set_tab_label_text(mm.widget(), "Main menu");

        window.show();

        Self {
            window,
            notebook,
            offline_game_tabs: Slab::new(),
            remote_tabs: Vec::new(),
            relm: relm.clone(),
        }
    }
}
