use gtk::{ButtonExt, ContainerExt, WidgetExt};
use relm::{Relm, Update, Widget};

pub trait UserActionHandler {
    fn on_start_hotseat(&self);
    fn on_go_online(&self);
    fn on_exit(&self);
}

pub struct Component<H> {
    root: gtk::Box,
    marker: std::marker::PhantomData<H>,
}

#[derive(Msg)]
pub enum Void {}

impl<H> Update for Component<H> {
    type Model = (H,);
    type ModelParam = Self::Model;
    type Msg = Void;

    fn model(_relm: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, _event: Self::Msg) {}
}

impl<H> Widget for Component<H>
where
    H: UserActionHandler + 'static + Clone,
{
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.root.clone()
    }

    fn view(_relm: &Relm<Self>, (handler,): Self::Model) -> Self {
        let button_hotseat = gtk::Button::new_with_label("Start Offline Game");
        {
            let handler = handler.clone();
            button_hotseat.connect_clicked(move |_| handler.on_start_hotseat());
        }

        let button_online = gtk::Button::new_with_label("Play Online");
        {
            let handler = handler.clone();
            button_online.connect_clicked(move |_| handler.on_go_online());
        }

        let button_exit = gtk::Button::new_with_label("Exit Game");
        button_exit.connect_clicked(move |_| handler.on_exit());

        let root = gtk::Box::new(gtk::Orientation::Vertical, 0);
        root.add(&button_hotseat);
        root.add(&button_online);
        root.add(&button_exit);

        root.show_all();

        Self {
            root,
            marker: Default::default(),
        }
    }
}
