use crate::network::{self, Client, LobbyEvent, LobbyModel};
use crate::util::ListStoreIter;

use server::messages::*;

use relm::{Relm, Update, Widget};

use gtk::{
    self, ButtonExt, CellLayoutExt, ContainerExt, DialogExt, EntryExt, GtkListStoreExtManual,
    Orientation, StaticType, TreeModelExt, TreeSelectionExt, TreeViewColumnExt, TreeViewExt,
    WidgetExt,
};

use std::cell::RefCell;
use std::rc::Rc;

const TITLE_COLUMN: i32 = 0;
const ID_COLUMN: i32 = 1;

#[derive(Msg)]
pub enum Message {
    CreateLobby(String),
    JoinLobby,
    SelectedLobby(Option<LobbyId>),
    UpdateUi,
}

pub struct Component {
    root: gtk::Box,
    tree_view: gtk::TreeView,
    join_button: gtk::Button,

    client: Rc<RefCell<Client>>,
    lobbies: Rc<RefCell<LobbyModel>>,
    user_id: UserId,
}

impl Component {
    fn update_ui(&self, id: Option<LobbyId>) {
        let joinable = id
            .and_then(|id| {
                self.lobbies
                    .borrow()
                    .get_lobbies()
                    .get(&id)
                    .map(|lobby| lobby.guest.is_none() && lobby.owner != self.user_id)
            })
            .unwrap_or(false);

        self.join_button.set_sensitive(joinable);
    }
}

impl Update for Component {
    type Model = (
        Rc<RefCell<Client>>,
        Rc<RefCell<LobbyModel>>,
        UserId,
        gtk::Window,
    );
    type ModelParam = Self::Model;
    type Msg = Message;

    fn model(_: &Relm<Self>, model_param: Self::ModelParam) -> Self::Model {
        model_param
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            Message::CreateLobby(title) => {
                self.client.borrow_mut().create_lobby(title);
            }
            Message::JoinLobby => {
                if let Some(lobby_id) = self
                    .tree_view
                    .get_selection()
                    .get_selected()
                    .and_then(|(store, iter)| store.get_value(&iter, ID_COLUMN).get::<u32>())
                {
                    let lobby_id = LobbyId(lobby_id);
                    self.client.borrow_mut().join_lobby(lobby_id);
                }
            }
            Message::SelectedLobby(id) => {
                self.update_ui(id);
            }
            Message::UpdateUi => {
                let id = self
                    .tree_view
                    .get_selection()
                    .get_selected()
                    .and_then(|(store, iter)| store.get_value(&iter, ID_COLUMN).get::<u32>());

                self.update_ui(id.map(LobbyId));
            }
        }
    }
}

impl Widget for Component {
    type Root = gtk::Box;

    fn root(&self) -> Self::Root {
        self.root.clone()
    }

    fn view(relm: &Relm<Self>, (client, lobbies, user_id, window): Self::Model) -> Self {
        let root = gtk::Box::new(Orientation::Vertical, 0);

        root.set_margin_top(8);
        root.set_margin_left(8);
        root.set_margin_right(8);
        root.set_margin_bottom(8);

        let tree_view = gtk::TreeView::new();
        tree_view.set_headers_visible(false);
        tree_view.set_vexpand(true);
        let column = gtk::TreeViewColumn::new();
        let cell = gtk::CellRendererText::new();

        column.pack_start(&cell, true);
        column.add_attribute(&cell, "text", TITLE_COLUMN);
        column.set_title("Title");

        let store = gtk::ListStore::new(&[String::static_type(), u32::static_type()]);
        tree_view.set_model(Some(&store));

        let join = gtk::Button::new_with_label("Join selected lobby");
        join.set_sensitive(false);
        let create = gtk::Button::new_with_label("Create new lobby");

        connect!(relm, join, connect_clicked(_), Message::JoinLobby);

        root.add(&tree_view);
        root.add(&join);
        root.add(&create);

        let stream = relm.stream().clone();
        tree_view.connect_cursor_changed(move |t| {
            let id = t
                .get_selection()
                .get_selected()
                .and_then(|(store, iter)| store.get_value(&iter, ID_COLUMN).get::<u32>())
                .map(LobbyId);

            stream.emit(Message::SelectedLobby(id));
        });

        tree_view.append_column(&column);

        let new_lobby_dialog = gtk::MessageDialog::new(
            Some(&window),
            gtk::DialogFlags::all(),
            gtk::MessageType::Question,
            gtk::ButtonsType::OkCancel,
            "Enter a name for the new lobby",
        );

        new_lobby_dialog.connect_delete_event(|dialog, _event| {
            dialog.hide();
            gtk::Inhibit(true)
        });

        let dialog_content = new_lobby_dialog.get_content_area();
        let lobby_title_entry = gtk::Entry::new();
        lobby_title_entry.set_placeholder_text("Lobby title");
        lobby_title_entry.show();
        dialog_content.add(&lobby_title_entry);

        let stream = relm.stream().clone();
        new_lobby_dialog.connect_response(move |dialog, response| {
            if let gtk::ResponseType::Ok = response.into() {
                let title = lobby_title_entry.get_text().unwrap();
                stream.emit(Message::CreateLobby(title));
            }
            dialog.hide();
            lobby_title_entry.set_text("");
        });

        create.connect_clicked(move |_| new_lobby_dialog.show());

        for (id, lobby) in lobbies.borrow().get_lobbies().iter() {
            store.insert_with_values(
                None,
                &[TITLE_COLUMN as u32, ID_COLUMN as u32],
                &[&lobby.title, &(id.0 as u32)],
            );
        }

        {
            fn destroy_lobby(store: &gtk::ListStore, lobby_id: LobbyId) {
                if let Some(row) = ListStoreIter::new(&store).find(|row| {
                    row.get_column(ID_COLUMN).get::<u32>().unwrap() == lobby_id.0 as u32
                }) {
                    row.remove();
                }
            }

            let lobbies = lobbies.clone();
            let stream = relm.stream().clone();
            client.borrow_mut().stream().observe(move |msg| {
                if let network::ClientMessage::LobbyMessage(msg) = msg {
                    if let network::LobbyMessage::LobbyEvent {
                        lobby: lobby_id,
                        event,
                    } = msg
                    {
                        match event {
                            LobbyEvent::Created => {
                                let lobbies = lobbies.borrow();
                                let lobby = lobbies.get_lobbies().get(&lobby_id).unwrap();
                                store.insert_with_values(
                                    None,
                                    &[TITLE_COLUMN as u32, ID_COLUMN as u32],
                                    &[&lobby.title, &(lobby_id.0 as u32)],
                                );
                            }
                            LobbyEvent::Destroyed => {
                                destroy_lobby(&store, *lobby_id);
                            }
                            LobbyEvent::GuestChanged => {
                                stream.emit(Message::UpdateUi);
                            }
                            LobbyEvent::Started(_) => {
                                destroy_lobby(&store, *lobby_id);
                            }
                            LobbyEvent::TitleChanged => {
                                let iter = store.get_iter_first();

                                let result = match iter {
                                    None => None,
                                    Some(iter) => loop {
                                        let id =
                                            store.get_value(&iter, ID_COLUMN).get::<u32>().unwrap();

                                        if id == lobby_id.0 as u32 {
                                            break Some(iter);
                                        }

                                        if !store.iter_next(&iter) {
                                            break None;
                                        }
                                    },
                                };

                                if let Some(iter) = result {
                                    let lobbies = lobbies.borrow();
                                    let lobby = lobbies.get_lobbies().get(&lobby_id).unwrap();
                                    store.set_value(
                                        &iter,
                                        TITLE_COLUMN as u32,
                                        &(&lobby.title).into(),
                                    );
                                }
                            }
                        }
                    }
                }
            });
        }

        root.show_all();

        Component {
            root,
            tree_view,
            join_button: join,

            client,
            lobbies,
            user_id,
        }
    }
}
