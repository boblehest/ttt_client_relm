#[macro_use]
extern crate relm;
#[macro_use]
extern crate relm_derive;
#[macro_use]
extern crate log;

mod network;
mod util;
mod widgets;

use relm::Widget;

fn main() {
    simple_logger::init().unwrap();
    widgets::main_window::Component::run(()).unwrap();
}
